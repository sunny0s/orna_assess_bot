import tesserocr
from PIL import Image
import logging
from telegram.ext import Updater, CommandHandler, MessageHandler, CallbackQueryHandler, Filters
from io import BytesIO
import re
import requests
from telegram.parsemode import ParseMode
import json

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

r_name = re.compile(r"\n([\w ']{3,})\n")
r_hp   = re.compile(r"((?<=HP[:z!2]) *[-0-9o]{0,4})")
r_att  = re.compile(r"((?<=A[tr]t[:z!2]) *[-0-9o]{0,4})")
r_def  = re.compile(r"((?<=Def[:z!2]) *[-0-9o]{0,4})")
r_mag  = re.compile(r"((?<=Mag[:z!2]) *[-0-9o]{0,4})")
r_res  = re.compile(r"((?<=Res[:z!2]) *[-0-9o]{0,4})")
r_mana = re.compile(r"((?<=Mana[:z!2]) *[-0-9o]{0,4})")
r_dex  = re.compile(r"((?<=Dex[:z!2]) *[-0-9o]{0,4})")
r_ward = re.compile(r"((?<=Ward[:z!2]) *[-0-9o]{0,3})")
r_lvl  = re.compile(r"((?<=LEVEL) *[0-9o]{1,2})")
r_lvl_alt = re.compile(r"\*[0-9o]{1,2}\W+([0-9o]{1,2})\W+[0-9o]{2}\/[0-9o]{2}\/2[0o][0-9o]{2}")
r_lvl_exists = re.compile(r"\n(LEVEL)\n")


def start(update, context):
    update.message.reply_text('Привет! Пришли мне скрин шмотки в виде файла (несжатое), и я попробую его распознать.')


def error(update, context):
    logger.warning('Update "%s" caused error "%s"', update, context.error)

def anytext (update, context):
    update.message.reply_text('Я пока не реагирую ни на какой текст. '\
                              +'Пришли мне скрин шмотки в виде файла (несжатое), '\
                              +'и я попробую его распознать.\n')


def sentphoto (update, context):
    update.message.reply_text('Фото нужно в виде файла. Шакалы не дремлют.')

def searchattr (r, text, isint=True):
    res = ""
    g = re.search(r, text)
    if g is not None:
        res = g.groups()[0]
    res = res.strip()
    if isint:
        if \
            len(res)>4 or\
            len(res)>3 and res[0] !='1':
              if res.rfind('o') != -1:
                res = res[:res.rfind('o')]
              elif res.rfind('0') != -1:
                res = res[:res.rfind('0')]

        res=res.replace('o','0')
        
    return res

def filephoto (update, context):
    file=update.message.document.get_file()
    buff=file.download_as_bytearray()
    im = Image.open(BytesIO(buff))
    text=tesserocr.image_to_text(im)

    print(text)

    name_grp = re.findall(r_name, text)
    name = name_grp[0]
    if name.find ('Followers') != -1:
        name = name_grp[1]
    classes = {'Broken', 'Poor', 'Famed', 'Superior', 'Legendary', 'Ornate', 'Masterforged', 'Demonforged'}
    elements = {'Lightning', 'Fire', 'Earthen', 'Water', 'Holy', 'Dark'}
    quality = 'Normal'
    element = ''
    for c in classes:
        if name.find(c) != -1:
            quality = c
            name=name[len(c)+1:]
    for e in elements:
        pos = name.rfind(e)
        if (pos != -1) and (pos + len(e) == len(name)):
            element = e
            name = name[:name.rfind(e)-4]

    hp = searchattr(r_hp, text)
    att = searchattr(r_att, text)
    df = searchattr(r_def, text)
    mag = searchattr(r_mag, text)
    res = searchattr(r_res, text)
    mana = searchattr(r_mana, text)
    dex = searchattr(r_dex, text)
    ward = searchattr(r_ward, text)
    level =  searchattr(r_lvl, text)
    if level == "":
        #try alternate if level exists
        lvlexists = searchattr(r_lvl_exists, text, False)
        if lvlexists == 'LEVEL':
            level = searchattr(r_lvl_alt, text)
        logger.info("Alternative: '{}'; '{}'".format(lvlexists, level))
    j = {}
    j.update ({'name':name})
    if hp != '': j.update({'hp':int(hp)})
    if att != '': j.update({'attack': int(att)})
    if df != '': j.update({'defense': int(df)})
    if mag != '': j.update({'magic': int(mag)})
    if res != '': j.update({'resistance': int(res)})
    if mana != '': j.update({'mana': int(mana)})
    if dex != '': j.update({'dexterity': int(dex)})
    if ward != '': j.update({'ward': int(ward)})
    if level == '':
        level = '1'
    j.update({'level': int(level)})
    print(j)
    r = requests.post('https://orna.guide/api/v1/assess', json=j)

    data = r.json()
    print(data)

    if 'error' in data:
        update.message.reply_text(data['error'])
        update.message.bot.sendMessage(chat_id=29463816, text = text+json.dumps(j)+json.dumps(data))
        update.message.bot.sendDocument(chat_id=29463816, document=update.message.document)
        return
    name = 'Name: {}\n'.format(data['name']) if 'name' in data else ''
    desc = 'Description: {}\n'.format(data['description']) if 'description' in data else ''
    tier = 'Tier: {}\n'.format(data['tier']) if 'tier' in data else ''
    type = 'Type: {}\n'.format(data['type']) if 'type' in data else ''
    boss = 'Boss: {}\n'.format(data['boss']) if 'boss' in data else ''
    qty = 'Quality: {} ({})\n'.format(quality, data['quality']) if 'quality' in data\
        else 'Quality: {}\n'.format(quality)
    materials = 'Materials: {}\n'.format(', '.join([a['name'] for a in data['materials']]))\
        if 'materials' in data else ''
    equip = 'Equipped by: {}\n'.format(', '.join([a['name'] for a in data['equipped_by']])) \
        if 'equipped_by' in data else ''
    drop = 'Dropped by: {}\n'.format(', '.join([a['name'] for a in data['dropped_by']])) \
        if 'dropped_by' in data else ''

    h = ['level']
    h.extend(list(data['stats']))
    headers = ''.join(['{:>'+str(len(i)+1)+'}' for i in h]).format(*list(h))
    levels = {"level": {'base': 0, 'values': list(range(1, 13))}}
    levels.update(data['stats'])

    statstable = '\n'.join( \
        [''.join(['{:>'+str(len(i)+1)+'}' for i in h]) \
        .format(*[levels[stat]['values'][level] \
        for stat in h]) \
        for level in range(12)])

    statslist = {"HP": hp, "Att": att, "Def":df, "Mag": mag, "Res": res, "Mana": mana, \
                 "Dex": dex, "Ward" : ward}
    filtered_stats = {k:v for k, v in statslist.items()  if v != "" }

    reply = "\n{}{}{}{}{}{}{}{}".format(name, desc, materials, tier, type, boss, equip, drop)+ \
        ', '.join(k + ": " + v for k, v in filtered_stats.items()) + "\nLevel: {}".format(level)+ \
        "\n\n*ASSESSMENT*\n{}`{}\n{}`\n".format(qty, headers, statstable)

    logger.info(reply)
    update.message.reply_text(reply, parse_mode=ParseMode.MARKDOWN)


def main():

    updater = Updater("1066401394:AAGk5VyahMKCp5qvC607c_SObrJesPAgIEE", use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))

    dp.add_handler(MessageHandler(Filters.text, anytext))
    dp.add_handler(MessageHandler(Filters.photo, sentphoto))
    dp.add_handler(MessageHandler(Filters.document.category("image"), filephoto))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Block until you press Ctrl-C or the process receives SIGINT, SIGTERM or
    # SIGABRT. This should be used most of the time, since start_polling() is
    # non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
